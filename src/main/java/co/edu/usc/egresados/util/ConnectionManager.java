/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author davidaristizabal
 */
public class ConnectionManager {

    private static String url = "jdbc:mysql://localhost:3306/bd_egresados?useSSL=false";
    private static String driverName = "com.mysql.cj.jdbc.Driver";
    private static String username = "root";
    private static String password = "Pr0y3ct0c4sd*";
    private static Connection con;
    private static String urlstring;

    public static Connection getConnection() throws SQLException {
        try {
            Class.forName(driverName);
            try {
                con = DriverManager.getConnection(url, username, password);
            } catch (SQLException ex) {
                // log an exception. fro example:
                System.out.println("Failed to create the database connection." + ex.getMessage());
                ex.printStackTrace();
            }
        } catch (ClassNotFoundException ex) {
            // log an exception. for example:
            ex.printStackTrace();
            System.out.println("Driver not found.");
        }
        return con;
    }
}
