/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.util;

/**
 *
 * @author davidaristizabal
 */
public interface HTTPHeaderNames {
    public static final String SERVICE_KEY = "service_key";
    public static final String AUTH_TOKEN = "Authorization";
}
