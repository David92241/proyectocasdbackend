/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.filter;

import co.edu.usc.egresados.facade.UsuarioFacade;
import co.edu.usc.egresados.util.HTTPHeaderNames;
import java.io.IOException;
import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author anderson
 */
@Provider
@PreMatching
public class CrossOriginResourceSharingFilter implements ContainerRequestFilter, ContainerResponseFilter {
    
    private final static Logger log = Logger.getLogger(CrossOriginResourceSharingFilter.class.getName());
    
    @Override
    public void filter(ContainerRequestContext requestCtx) throws IOException {
        String path = requestCtx.getUriInfo().getPath();
        log.info("Filtering request path: " + path);

        // IMPORTANT!!! First, Acknowledge any pre-flight test from browsers for this case before validating the headers (CORS stuff)
        log.info(requestCtx.getRequest().getMethod());
        if (requestCtx.getRequest().getMethod().equals("OPTIONS")) {
            requestCtx.abortWith(Response.ok().build());
            return;
        }

        // Then check is the service key exists and is valid.
        UsuarioFacade facade = new UsuarioFacade();
        String serviceKey = requestCtx.getHeaderString(HTTPHeaderNames.SERVICE_KEY);

//        if (!demoAuthenticator.isServiceKeyValid(serviceKey)) {
//            // Kick anyone without a valid service key
//            requestCtx.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
//
//            return;
//        }

        // For any pther methods besides login, the authToken must be verified
        if (!path.startsWith("Usuario/login/")) {
            log.info("1");
            String authToken = requestCtx.getHeaderString(HTTPHeaderNames.AUTH_TOKEN);
            log.info("2 "+authToken);
            // if it isn't valid, just kick them out.
            if (!facade.isValidToken(authToken)) {
                log.info("3 invalido");
                requestCtx.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            }
            log.info("4");
        }
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext response) {
        response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
        response.getHeaders().putSingle("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
        response.getHeaders().putSingle("Access-Control-Allow-Headers", "Content-Type");
    }
    
}
