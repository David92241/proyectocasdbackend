/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.main;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

/**
 *
 * @author davidaristizabal
 */

@ApplicationPath("rest")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        packages("co.edu.usc.egresados.main;co.edu.usc.egresados.service;co.edu.usc.egresados.filter");
        // Enable Tracing support.
        register(MultiPartFeature.class);

        property(ServerProperties.TRACING, "ALL");
    }

}
