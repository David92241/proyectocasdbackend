/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author ruber
 */
@Path("upload")
@Produces(MediaType.APPLICATION_JSON)
public class UploadFileService {

    //Se debe cambiar por la URL donde se quiera guardar la imagen
    private static final String UPLOAD_FOLDER = "/tmp/proyectocasdbackend/";

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream in,
            @FormDataParam("file") FormDataContentDisposition info) throws IOException {
        ObjectNode response = new ObjectMapper().createObjectNode();
        System.out.println("in>>" + in);
        File upload = new File(UPLOAD_FOLDER.concat(info.getFileName()));

        try {
            if (upload.exists()) {
                response.put("message", "file: " + upload.getName() + " already exists");
                return Response.status(Response.Status.CONFLICT).entity(response.toString()).build();
            } else {
                response.put("message", "Se ha creado el archivo con nombre: "+upload.getName());
                Files.copy(in, upload.toPath());
                return Response.status(Response.Status.OK).entity(response.toString()).build();
            }
        } catch (IOException e) {
            e.printStackTrace();
            response.put("message", e.getMessage());
            return Response.status(Response.Status.CONFLICT).entity(response.toString()).build();
        }
    }

//    @POST
//    @Path("/upload")
//    @Consumes("multipart/form-data")
//    public Response uploadFile(@MultipartForm FileUploadForm form) {
//
//        String fileName = "d:\\anything";
//
//        try {
//            writeFile(form.getData(), fileName);
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }
//
//        System.out.println("Done");
//
//        return Response.status(200)
//                .entity("uploadFile is called, Uploaded file name : " + fileName).build();
//
//    }
}
