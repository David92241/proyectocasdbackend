/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.service;

import co.edu.usc.egresados.entities.Instituciones;
import co.edu.usc.egresados.facade.InstitucionesFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author Mathews Jovel Castro
 */
@Path("Instituciones")
public class InstitucionesService {

    InstitucionesFacade facade = null;
    private static final String UPLOAD_FOLDER = "/opt/";

    public InstitucionesService() {
        facade = new InstitucionesFacade();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response InsertInstitucion(Instituciones institucionesBean) {
        Instituciones respuesta = facade.insertInstitucion(institucionesBean);
        return Response.ok(respuesta).build();
    }

    @POST
    @Path("findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Instituciones> respuesta = facade.findAllInstituciones();
            String json = objectMapper.writeValueAsString(respuesta);
            return Response.ok(json).build();

        } catch (JsonProcessingException ex) {
            Logger.getLogger(InstitucionesService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("updateInstitucion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateInstitucion(Instituciones institucionesBean) {
        Instituciones respuesta = facade.updateInstitucion(institucionesBean);
        return Response.ok(respuesta).build();
    }

    @POST
    @Path("deleteInstitucion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteInstitucion(Instituciones institucionesBean) {
        facade.deleteInstitucion(institucionesBean);
        return Response.ok().build();
    }

    @PUT
    @Path("uploadInstituciones")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadInstituciones(@FormDataParam("file") InputStream in,
            @FormDataParam("file") FormDataContentDisposition info) throws IOException {

        List<Instituciones> lstInstituciones = new ArrayList<>();
        Instituciones institucion = null;
        String splitBy = ",";
        String line = "";
        File upload = new File(UPLOAD_FOLDER.concat(info.getFileName()));
        BufferedReader br = null;
        try {

            Files.copy(in, upload.toPath());
            br = new BufferedReader(new FileReader(upload));
            //Se lee la cabecera
            br.readLine();
            while ((line = br.readLine()) != null) {
                if (!line.equals("")) {
                    institucion = new Instituciones();
                    // use comma as separator
                    String[] institucionLine = line.split(splitBy);
                    institucion.setComuna(institucionLine[0]);
                    institucion.setCodigoDane(institucionLine[1]);
                    institucion.setNombre(institucionLine[2]);
                    institucion.setEspecialidad(institucionLine[3]);
                    institucion.setNivelEducativo(institucionLine[4]);
                    institucion.setBarrio(institucionLine[5]);
                    institucion.setDireccion(institucionLine[6]);
                    institucion.setTelefono(institucionLine[7]);

                    lstInstituciones.add(institucion);
                }

            }

        } catch (FileNotFoundException e) {
            System.out.println("line>>>> " + line);
            upload.delete();
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("line1>>>> " + line);
            upload.delete();
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    upload.delete();
                    e.printStackTrace();
                }
            }
        }
        upload.delete();
        facade.uploadInstituciones(lstInstituciones);

        return Response.ok(info.getFileName()).build();

    }
}
