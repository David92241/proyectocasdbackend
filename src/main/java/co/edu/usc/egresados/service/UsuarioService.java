/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.service;

import co.edu.usc.egresados.entities.Usuario;
import co.edu.usc.egresados.facade.UsuarioFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Mathews Jovel Castro
 */
@Path("Usuario")
public class UsuarioService {

    UsuarioFacade facade = null;

    public UsuarioService() {
        facade = new UsuarioFacade();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response InsertUsuario(Usuario usuarioBean) {
        Usuario respuesta = facade.insertUsuario(usuarioBean);
        return Response.ok(respuesta).build();
    }

    @POST
    @Path("findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Usuario> respuesta = facade.findAllUsuarios();
            String json = objectMapper.writeValueAsString(respuesta);
            return Response.ok(json).build();

        } catch (JsonProcessingException ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("updateUsuario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUsuario(Usuario usuarioBean) {
        Usuario respuesta = facade.updateUsuario(usuarioBean);
        return Response.ok(respuesta).build();
    }

    @POST
    @Path("deleteUsuario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUsuario(Usuario usuarioBean) {
        facade.deleteUsuario(usuarioBean);
        return Response.ok().build();
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(Usuario usuarioBean) {
        Usuario respuesta = facade.login(usuarioBean);
        if (respuesta != null) {
            return Response.ok(respuesta).build();
        } else {
            return Response.noContent().build();
        }
    }
}
