/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.service;

import co.edu.usc.egresados.entities.Especialidad;
import co.edu.usc.egresados.facade.EspecialidadFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author davidaristizabal
 */
@Path("Especialidad")
public class EspecialidadService {

    EspecialidadFacade facade = null;

    public EspecialidadService() {
        facade = new EspecialidadFacade();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertEspecialidad(Especialidad especialidadBean) {
        Especialidad respuesta = facade.insertEspecialidad(especialidadBean);
        return Response.ok(respuesta).build();
    }

    @POST
    @Path("findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Especialidad> respuesta = facade.findAllEspecialidades();
            String json = objectMapper.writeValueAsString(respuesta);
            return Response.ok(json).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(EspecialidadService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("updateEspecialidad")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateEspecialidad(Especialidad especialidadBean) {
        Especialidad respuesta = facade.updateEspecialidad(especialidadBean);
        return Response.ok(respuesta).build();
    }

    @POST
    @Path("deleteEspecialidad")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteEspecialidad(Especialidad especialidadBean) {
        facade.deleteEspecialidad(especialidadBean);
        return Response.ok().build();
    }
}
