/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.service;

import co.edu.usc.egresados.entities.Barrio;
import co.edu.usc.egresados.facade.BarrioFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author Mathews Jovel Castro
 */
@Path("Barrio")
public class BarrioService {
    BarrioFacade facade = null;
    //private static final String UPLOAD_FOLDER = "/opt/";
    private static final String UPLOAD_FOLDER = "D:\\Users\\Usuario\\Documents";
    
    public BarrioService() {
        facade = new BarrioFacade();
    }
    //-------------------------------------------------------------------------------
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response InsertBarrio(Barrio barrioBean){
        Barrio respuesta = facade.insertBarrio(barrioBean);
        return Response.ok(respuesta).build();
    }
    //----------------------------------------------------------------------------------
    @POST
    @Path("findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(){
        ObjectMapper objectMapper = new ObjectMapper();
        
        try {
            List<Barrio> respuesta = facade.findAllBarrios();
            String json = objectMapper.writeValueAsString(respuesta);
            return Response.ok(json).build();
            
        } catch (JsonProcessingException ex) {
            Logger.getLogger(BarrioService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
    //-------------------------------------------------------------------------------
    @POST
    @Path("updateBarrio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateBarrio(Barrio barrioBean){
        Barrio respuesta = facade.updateBarrio(barrioBean);
        return Response.ok(respuesta).build();
    }
    //----------------------------------------------------------------------------------
    @POST
    @Path("deleteBarrio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteBarrio(Barrio barrioBean){
        facade.deleteBarrio(barrioBean);
        return Response.ok().build();
    }
    //-----------------------------------------------------------------------------------
    @PUT
    @Path("uploadBarrio")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadBarrio(@FormDataParam("file") InputStream in, 
            @FormDataParam("file") FormDataContentDisposition info) throws IOException{
        
        List<Barrio> lstBarrios = new ArrayList<>();
        Barrio barrio = null;
        String splitBy = ";";
        String line = "";
        File upload = new File(UPLOAD_FOLDER.concat(info.getFileName()));
        BufferedReader br = null;
        
        try {
            Files.copy(in, upload.toPath());
            br = new BufferedReader(new FileReader(upload));
            //Se lee la cabecera
            br.readLine();
            while ((line = br.readLine()) !=null) {                
                if (!line.equals("")) {
                    barrio = new Barrio();
                    // use comma as separator
                    String[] barrioLine = line.split(splitBy);
                    barrio.setBarId(barrioLine[0]);
                    barrio.setBarNombre(barrioLine[1]);
                    
                    lstBarrios.add(barrio);
                }
            }
            
        } catch (FileNotFoundException e) {
            System.out.println("line>>>> " + line);
            upload.delete();
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("line1>>>> " + line);
            upload.delete();
            e.printStackTrace();
            
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    upload.delete();
                    e.printStackTrace();
                }
            }
        }
        upload.delete();
        facade.uploadBarrio(lstBarrios);
        return Response.ok(info.getFileName()).build();
    }
}
