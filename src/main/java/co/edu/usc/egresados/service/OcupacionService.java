/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.service;
import co.edu.usc.egresados.entities.Ocupacion;
import co.edu.usc.egresados.facade.OcupacionFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author Mathews Jovel Castro
 */
@Path("Ocupacion")
public class OcupacionService {
    //Creación de un objeto tipo ocupación facade.
    OcupacionFacade facade = null;
    //Declaración de variable estatica que contendra la ruta donde se encuentra el archivo de las ocupaciones.
    //private static final String UPLOAD_FOLDER = "/opt/" 
    private static final String UPLOAD_FOLDER = "D:\\Users\\Usuario\\Desktop\\";
    //Creacion de un constructor tipo ocupación facade.
    public OcupacionService(){
        facade = new OcupacionFacade();
    }
    
    /**
     * Metodo tipo post que inserta una nueva ocupación.
     * @POST
     * @param ocupacionBean
     * @return ocupacionBean
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response InsertOcupacion(Ocupacion ocupacionBean) {
        Ocupacion respuesta = facade.insertOcupacion(ocupacionBean);
        return Response.ok(respuesta).build();
    }
    
    @POST
    @Path("findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(){
        ObjectMapper objectMapper = new ObjectMapper();
        
        try {
            List<Ocupacion> respuesta = facade.findAllOcupaciones();
            String json = objectMapper.writeValueAsString(respuesta);
            return Response.ok(json).build();
            
        } catch (JsonProcessingException ex) {
            Logger.getLogger(OcupacionService.class.getName()).log(Level.SEVERE,  null, ex);
            return Response.serverError().build();
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    @POST
    @Path("updateOcupacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateOcupacion(Ocupacion ocupacionBean){
        Ocupacion respuesta = facade.updateOcupacion(ocupacionBean);
        return Response.ok(respuesta).build();
    }
    
    @POST
    @Path("deleteOcupacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteOcupacion(Ocupacion ocupacionBean){
        facade.deleteOcupacion(ocupacionBean);
        return Response.ok().build();
    }
    
    @PUT
    @Path("uploadOcupacion")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadOcupacion(@FormDataParam("file") InputStream in, 
            @FormDataParam("file") FormDataContentDisposition info)throws IOException{
        
        List<Ocupacion> lstOcupacion = new ArrayList<>();
        Ocupacion ocupacion = null;
         String splitBy = ";";
        String line = "";
        File upload = new File(UPLOAD_FOLDER.concat(info.getFileName()));
        System.out.println(info.getFileName());
        BufferedReader br = null;
        
        try {
            Files.copy(in, upload.toPath());
            br = new BufferedReader(new FileReader(upload));
            //Se lee la cabecera
            br.readLine();
            while ((line = br.readLine()) !=null) {
                System.out.println(line);
                if (!line.equals("")) {
                    ocupacion = new Ocupacion();
                    // use comma as separator
                    
                    String[] ocupacionLine = line.split(splitBy);
                    //ocupacion.setOcupaId(ocupacionLine[0]);
                    ocupacion.setOcupaSiou(ocupacionLine[0]);
                    ocupacion.setOcupaNombre(ocupacionLine[1]);
                    
                    lstOcupacion.add(ocupacion);
                } 
            }
        } catch (FileNotFoundException e) {
            System.out.println("line>>>> " + line);
            upload.delete();
            e.printStackTrace();
            
        } catch(IOException e){
            System.out.println("line1>>>> " + line);
            upload.delete();
            e.printStackTrace();
            
        } finally{
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    upload.delete();
                    e.printStackTrace();
                }
            }
        }
        upload.delete();
        facade.uploadOcupacion(lstOcupacion);
        return Response.ok(info.getFileName()).build();
    }
}
