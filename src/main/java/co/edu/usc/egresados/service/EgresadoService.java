/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.service;

import co.edu.usc.egresados.entities.Egresado;
import co.edu.usc.egresados.facade.EgresadoFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author davidaristizabal
 */
@Path("Egresados")
public class EgresadoService {
    
    private EgresadoFacade facade = null;
    private static final String UPLOAD_FOLDER = "/opt/";

    public EgresadoService() {
        facade = new EgresadoFacade();
    }
    
    @PUT
    @Path("uploadEgresados")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadInstituciones(@FormDataParam("file") InputStream in,
            @FormDataParam("file") FormDataContentDisposition info) throws IOException {

        List<Egresado> lstEgresados = new ArrayList<>();
        Egresado egresado = null;
        String splitBy = ",";
        String line = "";
        File upload = new File(UPLOAD_FOLDER.concat(info.getFileName()));
        BufferedReader br = null;
        try {

            Files.copy(in, upload.toPath());
            br = new BufferedReader(new FileReader(upload));
            //Se lee la cabecera
            br.readLine();
            while ((line = br.readLine()) != null) {
                if (!line.equals("")) {
                    egresado = new Egresado();
                    // use comma as separator
                    String[] institucionLine = line.split(splitBy);
                    egresado.setEgreNombre(institucionLine[0]);
                    egresado.setEgreGenero(institucionLine[1]);
                    egresado.setEgreTel(Integer.parseInt(institucionLine[2]));
                    egresado.setEgreDicc(institucionLine[3]);
                    egresado.setEgreInstProced(Integer.parseInt(institucionLine[4]));
                    egresado.setEgreAnoTermin(Integer.parseInt(institucionLine[5]));
                    egresado.setEgreOcupa(institucionLine[6]);
                    egresado.setEgreLugarOcupa(institucionLine[7]);
                    egresado.setEgreCorreo(institucionLine[8]);
                    egresado.setIdSedeCasd(Integer.parseInt(institucionLine[9]));
                    egresado.setIdEspecialidad(Integer.parseInt(institucionLine[10]));
                    egresado.setIdInstitucion(Integer.parseInt(institucionLine[11]));
                    egresado.setEgreIdentificacion(institucionLine[12]);
                    egresado.setEgreTipoIdentificacion(institucionLine[13]);

                    lstEgresados.add(egresado);
                }

            }

        } catch (FileNotFoundException e) {
            System.out.println("line>>>> " + line);
            upload.delete();
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("line1>>>> " + line);
            upload.delete();
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    upload.delete();
                    e.printStackTrace();
                }
            }
        }
        upload.delete();
        facade.uploadEgresados(lstEgresados);

        return Response.ok(info.getFileName()).build();

    }
    
    @POST
    @Path("findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Egresado> respuesta = facade.findAll();
            String json = objectMapper.writeValueAsString(respuesta);
            return Response.ok(json).build();

        } catch (JsonProcessingException ex) {
            Logger.getLogger(InstitucionesService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
}
