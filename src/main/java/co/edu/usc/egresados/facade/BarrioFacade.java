/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.facade;

import co.edu.usc.egresados.entities.Barrio;
import co.edu.usc.egresados.util.ConnectionManager;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mathews Jovel Castro
 */
public class BarrioFacade implements Serializable{
    //Metodo encargado de mostrar todos los barrios. 
    public List<Barrio> findAllBarrios(){
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        List<Barrio> lstBarrioBeans = null;
        
        try {
             //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //se hace uso de la variable "sql" para escribir la instancia sql que busca todas las instituciones
            sql.append("SELECT * FROM barrio");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior 
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            //Declara una variable tipo ResultSet que ejecuta la instruccion sql almcenada en la variable "stmt" declarada anteriormente
            ResultSet rs = stmt.executeQuery();
            //Instancia de un arreglo de tipo listInstitucionesBeans
            lstBarrioBeans = new ArrayList<>();
            
            while(rs.next()){
                
                Barrio barrioBean = new Barrio();
                
                barrioBean.setBarId(rs.getString(1));
                barrioBean.setBarNombre(rs.getString(2));
                //AGREGAR LOS BARRIOS A LA LISTA.
                lstBarrioBeans.add(barrioBean);
            }
            // cierra el query ejecutado por la verible "stmt" almacenado en la variable "rs"
            rs.close();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();
            
            return lstBarrioBeans;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }    
    }
    /***
     * Metodo encargado de insertar un nuevo barrio.
     * 
     * @param barrioBean, El nuevo barrio a insertar.
     * @return barrioBean, El nuevo barrio insertado. 
     */
    public Barrio insertBarrio(Barrio barrioBean){
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("INSERT INTO barrio (Bar_id, Bar_nombre)");
            sql.append("VALUES (?,?)");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, barrioBean.getBarId());
            stmt.setString(2, barrioBean.getBarNombre());
            
            //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();
            
            return barrioBean;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /***
     * Metodo encargado de actualizar un barrio dada su id.
     * 
     * @param barrioBean, el barrio a actualizar dada su id.
     * @return barrioBean, el barrio actualizado dada su id.
     */
    public Barrio updateBarrio(Barrio barrioBean){
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("UPDATE barrio SET Bar_nombre = ?");
            sql.append("WHERE Bar_id = ?");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, barrioBean.getBarId());
            stmt.setString(2, barrioBean.getBarNombre());
            //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();
            //Retorna el barrio actualizado por su id.
            return barrioBean;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        
    }
    /***
     * Metodo encargado de eliminar un barrio dada su id.
     * 
     * @param barrioBean, barrio a eliminar dada su id.
     * @return barrioBean, barrio eliminado por su id.
     */
    public Barrio deleteBarrio(Barrio barrioBean){
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("DELETE FROM barrio WHERE Bar_id = ?");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, barrioBean.getBarId());
            //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();
            //Retorna el barrio actualizado por su id.
            return barrioBean;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void uploadBarrio(List<Barrio> lstBarrio){
       //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("INSERT INTO barrio (Bar_id, Bar_nombre) VALUES");
            //Recorre la lista de barrios creada como parametro
            for(Barrio barrio: lstBarrio){
                sql.append("(");
                sql.append("'").append(barrio.getBarId()).append("', ");
                sql.append("'").append(barrio.getBarNombre()).append("'),");
            }
            
            con.prepareStatement(sql.substring(0, sql.length() - 1)).execute();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
