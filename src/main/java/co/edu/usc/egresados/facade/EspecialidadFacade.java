/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.facade;

import co.edu.usc.egresados.entities.Especialidad;
import co.edu.usc.egresados.util.ConnectionManager;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author davidaristizabal
 */
public class EspecialidadFacade implements Serializable {

    /**
     * Metodo encargado de buscar todas las especialidades
     * @return 
     */
    public List<Especialidad> findAllEspecialidades() {
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        List<Especialidad> lstEspecialidadBeans = null;
        try {
            con= ConnectionManager.getConnection();
            sql.append("SELECT * FROM especialidad");
            
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            ResultSet rs = stmt.executeQuery();
            
            lstEspecialidadBeans = new ArrayList<>();
            while(rs.next()){
                Especialidad especialidadBean = new Especialidad();
                especialidadBean.setEspeId(rs.getInt(1));
                especialidadBean.setIdSedeCasd(rs.getInt(2));
                especialidadBean.setEspeNombre(rs.getString(3));
                
                lstEspecialidadBeans.add(especialidadBean);
            }
            
            rs.close();
            stmt.close();
            con.close();
            
            return lstEspecialidadBeans;
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Metodo encargado de agregar una nueva especialidad
     * @param especialidadBean
     * @return 
     */
    public Especialidad insertEspecialidad(Especialidad especialidadBean){
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        try {
            con = ConnectionManager.getConnection();
            
            sql.append("INSERT INTO especialidad (id_sede_casd, Espe_nombre) ");
            sql.append(" VALUES (?, ?) ");
            
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setInt(1, especialidadBean.getIdSedeCasd());
            stmt.setString(2, especialidadBean.getEspeNombre());
            
            stmt.execute();
            
            stmt.close();
            con.close();
            return especialidadBean;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Metodo encargado de actualizar una especialidad
     * @param especialidadBean
     * @return 
     */
    public Especialidad updateEspecialidad(Especialidad especialidadBean){
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        try {
            con = ConnectionManager.getConnection();
            
            sql.append("UPDATE especialidad SET Espe_nombre = ? ");
            sql.append(" WHERE Espe_id = ?");
            
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, especialidadBean.getEspeNombre());
            stmt.setInt(2, especialidadBean.getEspeId());
            
            stmt.execute();
            
            stmt.close();
            con.close();
            return especialidadBean;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo encargado de borrar una especialidad
     * @param especialidadBean
     * @return 
     */
    public void deleteEspecialidad(Especialidad especialidadBean){
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        try {
            con = ConnectionManager.getConnection();
            
            sql.append("DELETE FROM especialidad WHERE Espe_id = ?");
            
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setInt(1, especialidadBean.getEspeId());
            
            stmt.execute();
            
            stmt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
