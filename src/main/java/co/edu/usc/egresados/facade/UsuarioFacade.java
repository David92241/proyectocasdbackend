/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.facade;

import co.edu.usc.egresados.entities.Usuario;
import co.edu.usc.egresados.util.ConnectionManager;
import co.edu.usc.egresados.util.Util;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Mathews Jovel Castro
 */
public class UsuarioFacade implements Serializable {

    /**
     * Metodo encargado de buscar todos los usuarios
     *
     * @return una lista con los usuarios
     */
    public List<Usuario> findAllUsuarios() {
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        List<Usuario> lstUsuariosBeans = null;
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //se hace uso de la variable "sql" para escribir la instancia sql que busca todas las instituciones
            sql.append("SELECT * FROM usuario");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior 
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            //Declara una variable tipo ResultSet que ejecuta la instruccion sql almcenada en la variable "stmt" declarada anteriormente
            ResultSet rs = stmt.executeQuery();
            //Instancia de un arreglo de tipo listInstitucionesBeans
            lstUsuariosBeans = new ArrayList<>();
            //Instacia de un recorrido while
            while (rs.next()) {
                //Creacion de una variable tipo Usuario
                Usuario usuarioBean = new Usuario();
                //Llamado del metodo setId de acuerdo con la id obtenida por la variable "rs"
                usuarioBean.setId(rs.getInt(1));
                //Llamado del metodo setNombre de acuerdo con la identificacion obtenida por la variable "rs"
                usuarioBean.setNombre(rs.getString(2));
                //Llamado del metodo setContrasena de acuerdo con la identificacion obtenida por la variable "rs"
                usuarioBean.setContrasena(rs.getString(3));
                //Llamado del metodo setTipo_usuario de acuerdo con la identificacion obtenida por la variable "rs"
                usuarioBean.setTipo_usuario(rs.getString(4));
                //Llamado del metodo setId_sede_casd de acuerdo con la identificacion obtenida por la variable "rs"
                usuarioBean.setId_sede_casd(rs.getInt(5));
                //Agrega toda la cadena contenida en la variable usuarioBean a la lista de usuarios "lstUsuariosBeans"
                lstUsuariosBeans.add(usuarioBean);
            }
            // cierra el query ejecutado por la verible "stmt" almacenado en la variable "rs"
            rs.close();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

            return lstUsuariosBeans;

        } catch (Exception e) {
            //imprime una mensaje en consola de la esception atrapada por la variable 'e'
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo encargado de agregar un nuevo usuario.
     *
     * @param usuarioBean, el usuario a agregar.
     * @return el usuario agredado.
     */
    public Usuario insertUsuario(Usuario usuarioBean) {
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("INSERT INTO usuario (nombre, contrasena, tipo_usuario, id_sede_casd)");
            sql.append("VALUES (?, ?, ?, ?)");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            //asigna el nombre insertado al campo nombre
            stmt.setString(1, usuarioBean.getNombre());
            //asigna la contraseña insertada en el campo contraseña
            stmt.setString(2, usuarioBean.getContrasena());
            //asgina el tipo de usuario en el campo tipo_usuario
            stmt.setString(3, usuarioBean.getTipo_usuario());
            //asigna la id de la sede del casd al campo id_sede_casd
            stmt.setInt(4, usuarioBean.getId_sede_casd());

            //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

            return usuarioBean;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo encargado de actualizar un usuario por su id.
     *
     * @param usuarioBean, el usuario a actualizar.
     * @return el usuario actualizado
     */
    public Usuario updateUsuario(Usuario usuarioBean) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();

        try {
            con = ConnectionManager.getConnection();

            sql.append("UPDATE usuario SET nombre = ?");
            sql.append("WHERE id = ?");

            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, usuarioBean.getNombre());
            stmt.setInt(2, usuarioBean.getId());

            stmt.execute();

            stmt.close();
            con.close();

            return usuarioBean;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * metodo encargado de eliminar un usuario por su id.
     *
     * @param usuarioBean, usuario a eliminar de la base de datos de usuarios.
     * @return la base de datos con el usuario eliminado.
     */
    public Usuario deleteUsuario(Usuario usuarioBean) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();

        try {
            con = ConnectionManager.getConnection();

            sql.append("DELETE FROM usuario WHERE id=?");

            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setInt(1, usuarioBean.getId());

            stmt.execute();

            stmt.close();
            con.close();

            return usuarioBean;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Usuario login(Usuario usuarioBean) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        Usuario respuesta = null;

        try {
            con = ConnectionManager.getConnection();

            sql.append("SELECT * FROM usuario WHERE nombre = ? AND contrasena = ?");

            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, usuarioBean.getNombre());
            stmt.setString(2, usuarioBean.getContrasena());

            System.out.println("nombre = " + usuarioBean.getNombre());
            System.out.println("pass = " + Util.generateSHA(usuarioBean.getContrasena()).toUpperCase());

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                respuesta = new Usuario();
                respuesta.setContrasena(rs.getString("contrasena"));
                respuesta.setId(rs.getInt("id"));
                respuesta.setTipo_usuario(rs.getString("tipo_usuario"));
                respuesta.setNombre(rs.getString("nombre"));

            }
            
            if(respuesta != null){
                String authToken = UUID.randomUUID().toString();
                respuesta.setToken(authToken);
                insertUsuarioLogin(authToken, respuesta.getId());
            }

            stmt.close();
            con.close();

            return respuesta;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private void insertUsuarioLogin(String authToken, Integer usuarioId){
        Connection con = null;
        StringBuilder sql = new StringBuilder();

        try {
            con = ConnectionManager.getConnection();
            sql.append("insert into UsuariosLogin values(?, ?, ?, true)");
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setInt(1, usuarioId);
            stmt.setDate(2, new Date(new java.util.Date().getTime()));
            stmt.setString(3, authToken);
            
            stmt.execute();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean isValidToken(String authToken){
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        boolean resultado = false;

        try {
            con = ConnectionManager.getConnection();
            sql.append("SELECT * FROM UsuariosLogin WHERE token = ? and valida = true");
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            
            stmt.setString(1, authToken);
            
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                resultado = true;
            }
            
            rs.close();
            stmt.close();
            
            return resultado;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void logout(Usuario usuarioBean){
        
    }

}
