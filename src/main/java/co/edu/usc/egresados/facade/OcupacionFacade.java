/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.facade;
import co.edu.usc.egresados.entities.Ocupacion;
import co.edu.usc.egresados.util.ConnectionManager;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Mathews Jovel Castro
 */
public class OcupacionFacade implements Serializable {

    /**
     * *
     * Metodo que muestra todas las ocupaciones
     *
     * @return lstOcupacionesBeans, la lista de ocupaciones a mostrar
     */
    public List<Ocupacion> findAllOcupaciones() {
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        List<Ocupacion> lstOcupacionesBeans = null;
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //se hace uso de la variable "sql" para escribir la instancia sql que busca todas las instituciones
            sql.append("SELECT * FROM ocupacion");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior 
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            //Declara una variable tipo ResultSet que ejecuta la instruccion sql almcenada en la variable "stmt" declarada anteriormente
            ResultSet rs = stmt.executeQuery();
            //Instancia de un arreglo de tipo listOcupacionesBeans
            lstOcupacionesBeans = new ArrayList<>();

            while (rs.next()) {
                //Creación de un objeto de tipo ocupación.
                Ocupacion ocupacionBean = new Ocupacion();
                //Llamado de los metodos set de acuerdo con la id obtenida por la variable "rs"
                ocupacionBean.setOcupaId(rs.getInt(1));
                ocupacionBean.setOcupaSiou(rs.getString(2));
                ocupacionBean.setOcupaNombre(rs.getString(3));
                //Agrega la ocupacion a la lista.
                lstOcupacionesBeans.add(ocupacionBean);
            }
            // cierra el query ejecutado por la verible "stmt" almacenado en la variable "rs"
            rs.close();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

        } catch (Exception e) {
            //imprime una mensaje en consola de la esception atrapada por la variable 'e'
            e.printStackTrace();
            return null;
        }
        return lstOcupacionesBeans;
    }
    
    /**
     * Metodo que se encarga de insertar una nueva ocupación en la base de datos
     * @param ocupacionBean, la ocupación a insertar
     * @return ocupacionBean, la ocupación insertada.
     */
    public Ocupacion insertOcupacion(Ocupacion ocupacionBean) {
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("INSERT INTO ocupacion (ocupa_siou, ocupa_nombre)");
            sql.append("VALUES (?, ?)");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            //stmt.setInt(1, ocupacionBean.getOcupaId());
            stmt.setString(2, ocupacionBean.getOcupaSiou());
            stmt.setString(3, ocupacionBean.getOcupaNombre());

            //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return ocupacionBean;
    }
    
    /**
     * 
     * @param ocupacionBean
     * @return 
     */
    public Ocupacion updateOcupacion(Ocupacion ocupacionBean) {

        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("UPDATE ocupacion SET ocupa_siou = ?, ocupa_nombre = ?");
            sql.append("WHERE ocupa_id = ? ");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, ocupacionBean.getOcupaSiou());
            stmt.setString(2, ocupacionBean.getOcupaNombre());
            stmt.setInt(3, ocupacionBean.getOcupaId());

            //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return ocupacionBean;
    }
    
    public Ocupacion deleteOcupacion(Ocupacion ocupacionBean){
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
             //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("DELETE FROM ocupacion WHERE ocupa_id = ? ");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setInt(1, ocupacionBean.getOcupaId());
             //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return ocupacionBean;
    }
    
    public void uploadOcupacion(List<Ocupacion> lstOcupaciones) {
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("INSERT INTO ocupacion (ocupa_siou, ocupa_nombre) VALUES");
            //Recorre la lista de ocupaciones creada como parametro
            for (Ocupacion ocupacion : lstOcupaciones) {
                sql.append("(");
                sql.append("'").append(ocupacion.getOcupaSiou()).append("', ");
                sql.append("'").append(ocupacion.getOcupaNombre()).append("'),");
            }

            System.out.println(sql.substring(0, sql.length() - 1));
            con.prepareStatement(sql.substring(0, sql.length() - 1)).execute();
            
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
