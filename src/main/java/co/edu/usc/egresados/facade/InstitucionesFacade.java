/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.facade;

import co.edu.usc.egresados.entities.Instituciones;
import co.edu.usc.egresados.util.ConnectionManager;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mathews Jovel Castro
 */
public class InstitucionesFacade implements Serializable {

    /**
     * Metodo encargado de buscar todas las
     * instituciones----------------------------------
     *
     * @return una lista de instituciones
     */
    public List<Instituciones> findAllInstituciones() {
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        List<Instituciones> lstInstitucionesBeans = null;
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //se hace uso de la variable "sql" para escribir la instancia sql que busca todas las instituciones
            sql.append("SELECT * FROM instituciones");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior 
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            //Declara una variable tipo ResultSet que ejecuta la instruccion sql almcenada en la variable "stmt" declarada anteriormente
            ResultSet rs = stmt.executeQuery();
            //Instancia de un arreglo de tipo listInstitucionesBeans
            lstInstitucionesBeans = new ArrayList<>();
            //Instacia de un recorrido while
            while (rs.next()) {
                //Creacion de una variable tipo Instituciones
                Instituciones institucionesBean = new Instituciones();
                //Llamado del metodo setId de acuerdo con la id obtenida por la variable "rs"
                institucionesBean.setId(rs.getInt(1));
                institucionesBean.setComuna(rs.getString(2));
                institucionesBean.setNombre(rs.getString(3));
                institucionesBean.setCodigoDane(rs.getString(4));
                institucionesBean.setEspecialidad(rs.getString(5));
                institucionesBean.setNivelEducativo(rs.getString(6));
                institucionesBean.setBarrio(rs.getString(7));
                institucionesBean.setDireccion(rs.getString(8));
                institucionesBean.setTelefono(rs.getString(9));
                lstInstitucionesBeans.add(institucionesBean);
            }
            // cierra el query ejecutado por la verible "stmt" almacenado en la variable "rs"
            rs.close();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

            return lstInstitucionesBeans;

        } catch (Exception e) {
            //imprime una mensaje en consola de la esception atrapada por la variable 'e'
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo encargado de agregar una nueva institucion
     *
     * @param institucionesBean
     * @return la instituciones creadas
     */
    public Instituciones insertInstitucion(Instituciones institucionesBean) {
        //Instancia la variable tipo coneccion de la base de datos
        Connection con = null;
        //Instancia la variable sql de tipo stringBuilder
        StringBuilder sql = new StringBuilder();
        //Apertura de la clausula try y catch para el manejo de excepciones
        try {
            //se hace uso de la variable "con" para llamar el metodo getConnection de la clase ConnectionManager que obtiene la conexion con la bd
            con = ConnectionManager.getConnection();
            //instancia sql por medio de la varible "sql" de tipo StringBuilder
            sql.append("INSERT INTO instituciones (comuna, nombre, codigo_dane, especialidad, nivel_educativo, barrio, direccion, telefono)");
            sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            //declara una variable tipo  PreparedStatement que pre-almacena la instruccion sql anterior
            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, institucionesBean.getComuna());
            stmt.setString(2, institucionesBean.getNombre());
            stmt.setString(3, institucionesBean.getCodigoDane());
            stmt.setString(4, institucionesBean.getEspecialidad());
            stmt.setString(5, institucionesBean.getNivelEducativo());
            stmt.setString(6, institucionesBean.getBarrio());
            stmt.setString(7, institucionesBean.getDireccion());
            stmt.setString(8, institucionesBean.getTelefono());

            //ejecuta la sentencia sql almacenada en la varible "stmt"
            stmt.execute();
            //cierra la sentencia sql almacenada en la varible "stmt"
            stmt.close();
            //Cierra la conexion a la bd almacenada en la varible "con"
            con.close();

            return institucionesBean;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo encargado de actualizar una institucion
     *
     * @param institucionesBean
     * @return la institucion actualizada
     */
    public Instituciones updateInstitucion(Instituciones institucionesBean) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();

        try {
            con = ConnectionManager.getConnection();

            sql.append("UPDATE instituciones SET nombre = ?, comuna = ?, codigo_dane = ?, especialidad = ?, nivel_educativo = ?, barrio = ?, direccion = ?, telefono = ? ");
            sql.append("WHERE id = ?");

            PreparedStatement stmt = con.prepareStatement(sql.toString());
            stmt.setString(1, institucionesBean.getNombre());
            stmt.setString(2, institucionesBean.getComuna());
            stmt.setString(3, institucionesBean.getCodigoDane());
            stmt.setString(4, institucionesBean.getEspecialidad());
            stmt.setString(5, institucionesBean.getNivelEducativo());
            stmt.setString(6, institucionesBean.getBarrio());
            stmt.setString(7, institucionesBean.getDireccion());
            stmt.setString(8, institucionesBean.getTelefono());
            stmt.setInt(9, institucionesBean.getId());

            stmt.execute();

            stmt.close();
            con.close();

            return institucionesBean;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Instituciones deleteInstitucion(Instituciones institucionesBean) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();

        try {
            con = ConnectionManager.getConnection();

            sql.append("DELETE FROM instituciones WHERE id = ?");

            PreparedStatement stmt = con.prepareStatement(sql.toString());

            stmt.setInt(1, institucionesBean.getId());

            stmt.execute();

            stmt.close();
            con.close();

            return institucionesBean;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void uploadInstituciones(List<Instituciones> lstInstituciones) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        try {
            con = ConnectionManager.getConnection();
            sql.append("INSERT INTO instituciones (comuna, nombre, codigo_dane, especialidad, nivel_educativo, barrio, direccion, telefono) VALUES ");
            for (Instituciones instituciones : lstInstituciones) {
                sql.append("(");
                sql.append("'").append(instituciones.getComuna()).append("', ");
                sql.append("'").append(instituciones.getNombre()).append("', ");
                sql.append("'").append(instituciones.getCodigoDane()).append("', ");
                sql.append("'").append(instituciones.getEspecialidad()).append("', ");
                sql.append("'").append(instituciones.getNivelEducativo()).append("', ");
                sql.append("'").append(instituciones.getBarrio()).append("', ");
                sql.append("'").append(instituciones.getDireccion()).append("', ");
                sql.append("'").append(instituciones.getTelefono()).append("'),");
            }
            
            con.prepareStatement(sql.substring(0, sql.length() - 1)).execute();
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
