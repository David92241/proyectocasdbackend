/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.facade;

import co.edu.usc.egresados.entities.Egresado;
import co.edu.usc.egresados.util.ConnectionManager;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author davidaristizabal
 */
public class EgresadoFacade implements Serializable {

    public void uploadEgresados(List<Egresado> lstEgresados) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        try {
            con = ConnectionManager.getConnection();
            sql.append("INSERT INTO egresado (Egre_nombre, Egre_genero, Egre_tel, Egre_dicc, Egre_inst_proced, Egre_ano_termin, Egre_ocupa, Egre_lugar_ocupa, Egre_correo, egre_identificacion, egre_tipoIdentificacion) VALUES ");
            for (Egresado instituciones : lstEgresados) {
                sql.append("(");
                sql.append("'").append(instituciones.getEgreNombre()).append("', ");
                sql.append("'").append(instituciones.getEgreGenero()).append("', ");
                sql.append("'").append(instituciones.getEgreTel()).append("', ");
                sql.append("'").append(instituciones.getEgreDicc()).append("', ");
                sql.append("'").append(instituciones.getEgreInstProced()).append("', ");
                sql.append("'").append(instituciones.getEgreAnoTermin()).append("', ");
                sql.append("'").append(instituciones.getEgreOcupa()).append("', ");
                sql.append("'").append(instituciones.getEgreLugarOcupa()).append("',");
                sql.append("'").append(instituciones.getEgreCorreo()).append("'),");
                sql.append("'").append(instituciones.getEgreIdentificacion()).append("'),");
                sql.append("'").append(instituciones.getEgreTipoIdentificacion()).append("')");
            }

            con.prepareStatement(sql.substring(0, sql.length() - 1)).execute();
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Egresado> findAll() {
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        List<Egresado> lstEgresados = new ArrayList<>();
        try {
            con = ConnectionManager.getConnection();
            sql.append("SELECT * FROM egresado");

            PreparedStatement stmt = con.prepareStatement(sql.toString());
            ResultSet rs = stmt.executeQuery();

            Egresado egresado = null;
            while (rs.next()) {
                egresado = new Egresado();
                egresado.setEgreId(rs.getInt(1));
                egresado.setEgreNombre(rs.getString(2));
                egresado.setEgreGenero(rs.getString(3));
                egresado.setEgreTel(rs.getInt(4));
                egresado.setEgreDicc(rs.getString(5));
                egresado.setEgreInstProced(rs.getInt(6));
                egresado.setEgreAnoTermin(rs.getInt(7));
                egresado.setEgreOcupa(rs.getString(8));
                egresado.setEgreLugarOcupa(rs.getString(9));
                egresado.setEgreCorreo(rs.getString(10));
                egresado.setIdSedeCasd(rs.getInt(11));
                egresado.setIdEspecialidad(rs.getInt(12));
                egresado.setIdInstitucion(rs.getInt(13));
                egresado.setEgreIdentificacion(rs.getString(14));
                egresado.setEgreTipoIdentificacion(rs.getString(15));

                lstEgresados.add(egresado);
            }

            rs.close();
            stmt.close();
            con.close();

            return lstEgresados;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Egresado deleteEgresado(Egresado egresado) {
        Connection con = null;
        StringBuilder sql = new StringBuilder();
        try {
            con = ConnectionManager.getConnection();
            sql.append("DELETE FROM egresado WHERE Egre_id = ?");

            PreparedStatement stmt = con.prepareStatement(sql.toString());

            stmt.setInt(1, egresado.getEgreId());

            stmt.execute();

            stmt.close();
            con.close();

            return egresado;
        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }
}
