/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.entities;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davidaristizabal
 */
@XmlRootElement
public class Especialidad {
    private Integer espeId;
    private Integer idSedeCasd;
    private String espeNombre;

    public Especialidad() {
    }

    public Especialidad(Integer espeId, Integer idSedeCasd, String espeNombre) {
        this.espeId = espeId;
        this.idSedeCasd = idSedeCasd;
        this.espeNombre = espeNombre;
    }

    public Integer getEspeId() {
        return espeId;
    }

    public void setEspeId(Integer espeId) {
        this.espeId = espeId;
    }

    public Integer getIdSedeCasd() {
        return idSedeCasd;
    }

    public void setIdSedeCasd(Integer idSedeCasd) {
        this.idSedeCasd = idSedeCasd;
    }

    public String getEspeNombre() {
        return espeNombre;
    }

    public void setEspeNombre(String espeNombre) {
        this.espeNombre = espeNombre;
    }
    
}
