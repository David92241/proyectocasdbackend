/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.entities;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davidaristizabal
 */
@XmlRootElement
public class Egresado {
    
    private Integer egreId;
    private String egreNombre;
    private String egreGenero;
    private Integer egreTel;
    private String egreDicc;
    private Integer egreInstProced;
    private Integer egreAnoTermin;
    private String egreOcupa;
    private String egreLugarOcupa;
    private String egreCorreo;
    private Integer idSedeCasd;
    private Integer idEspecialidad;
    private Integer idInstitucion;
    private String egreIdentificacion;
    private String egreTipoIdentificacion;

    public Egresado() {
    }

    public Egresado(Integer egreId, String egreNombre, String egreGenero, Integer egreTel, String egreDicc, Integer egreInstProced, Integer egreAnoTermin, String egreOcupa, String egreLugarOcupa, String egreCorreo, Integer idSedeCasd, Integer idEspecialidad, Integer idInstitucion) {
        this.egreId = egreId;
        this.egreNombre = egreNombre;
        this.egreGenero = egreGenero;
        this.egreTel = egreTel;
        this.egreDicc = egreDicc;
        this.egreInstProced = egreInstProced;
        this.egreAnoTermin = egreAnoTermin;
        this.egreOcupa = egreOcupa;
        this.egreLugarOcupa = egreLugarOcupa;
        this.egreCorreo = egreCorreo;
        this.idSedeCasd = idSedeCasd;
        this.idEspecialidad = idEspecialidad;
        this.idInstitucion = idInstitucion;
    }

    public Integer getEgreId() {
        return egreId;
    }

    public void setEgreId(Integer egreId) {
        this.egreId = egreId;
    }

    public String getEgreNombre() {
        return egreNombre;
    }

    public void setEgreNombre(String egreNombre) {
        this.egreNombre = egreNombre;
    }

    public String getEgreGenero() {
        return egreGenero;
    }

    public void setEgreGenero(String egreGenero) {
        this.egreGenero = egreGenero;
    }

    public Integer getEgreTel() {
        return egreTel;
    }

    public void setEgreTel(Integer egreTel) {
        this.egreTel = egreTel;
    }

    public String getEgreDicc() {
        return egreDicc;
    }

    public void setEgreDicc(String egreDicc) {
        this.egreDicc = egreDicc;
    }

    public Integer getEgreInstProced() {
        return egreInstProced;
    }

    public void setEgreInstProced(Integer egreInstProced) {
        this.egreInstProced = egreInstProced;
    }

    public Integer getEgreAnoTermin() {
        return egreAnoTermin;
    }

    public void setEgreAnoTermin(Integer egreAnoTermin) {
        this.egreAnoTermin = egreAnoTermin;
    }

    public String getEgreOcupa() {
        return egreOcupa;
    }

    public void setEgreOcupa(String egreOcupa) {
        this.egreOcupa = egreOcupa;
    }

    public String getEgreLugarOcupa() {
        return egreLugarOcupa;
    }

    public void setEgreLugarOcupa(String egreLugarOcupa) {
        this.egreLugarOcupa = egreLugarOcupa;
    }

    public String getEgreCorreo() {
        return egreCorreo;
    }

    public void setEgreCorreo(String egreCorreo) {
        this.egreCorreo = egreCorreo;
    }

    public Integer getIdSedeCasd() {
        return idSedeCasd;
    }

    public void setIdSedeCasd(Integer idSedeCasd) {
        this.idSedeCasd = idSedeCasd;
    }

    public Integer getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public Integer getIdInstitucion() {
        return idInstitucion;
    }

    public void setIdInstitucion(Integer idInstitucion) {
        this.idInstitucion = idInstitucion;
    }    

    public String getEgreIdentificacion() {
        return egreIdentificacion;
    }

    public void setEgreIdentificacion(String egreIdentificacion) {
        this.egreIdentificacion = egreIdentificacion;
    }

    public String getEgreTipoIdentificacion() {
        return egreTipoIdentificacion;
    }

    public void setEgreTipoIdentificacion(String egreTipoIdentificacion) {
        this.egreTipoIdentificacion = egreTipoIdentificacion;
    }
    
}
