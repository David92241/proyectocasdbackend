/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.entities;

/**
 *
 * @author Mathews Jovel Castro
 */
public class Ocupacion {

//Atributos de la clase ocupación------------------------------
    /*
    Atributo que contendra el id de la ocupación
    */
    private int ocupaId;
    /*
    Atributo que contendra el codigo SIOU de la ocupación.
    */
    private String ocupaSiou;
    /*
    Atributo que contendra el nombre de las ocupaciones.
    */
    private String ocupaNombre;
    
    public Ocupacion(){
        
    }
    /**
     * Constructor de la clase Ocupación
     * @param ocupaId
     * @param ocupaSiou
     * @param ocupaNombre 
     */
    public Ocupacion(int ocupaId, String ocupaSiou, String ocupaNombre) {
        this.ocupaId = ocupaId;
        this.ocupaSiou = ocupaSiou;
        this.ocupaNombre = ocupaNombre;
    }

    /**
     * Metodo que retorna la id de la ocupación.
     * @return ocupaid, la id de la ocupación.
     */
    public int getOcupaId() {
        return ocupaId;
    }
    
    /**
     * Metodo que asigna una id para la ocupación.
     * @param ocupaId, la id de la ocupación a asignar. 
     */
    public void setOcupaId(int ocupaId) {
        this.ocupaId = ocupaId;
    }
    
    /**
     * Metodo que retorna el codigo SIOU de la ocupación.
     * @return ocupaSiou, el codigo SIOU de la ocupación.
     */
    public String getOcupaSiou() {
        return ocupaSiou;
    }
    
    /**
     * Metodo que asigna un codigo SIOU a la ocupación.
     * @param ocupaSiou, el codigo SIOU a asignar. 
     */
    public void setOcupaSiou(String ocupaSiou) {
        this.ocupaSiou = ocupaSiou;
    }
    
    /**
     * Metodo que retorna el nombre de la ocupación.
     * @return ocupaNombre, el nombre de la ocupación.
     */
    public String getOcupaNombre() {
        return ocupaNombre;
    }
    
    /**
     * Metodo que asigna un nombre a la ocupación.
     * @param ocupaNombre , el nombre de la ocupación a asignar.
     */
    public void setOcupaNombre(String ocupaNombre) {
        this.ocupaNombre = ocupaNombre;
    }
}
