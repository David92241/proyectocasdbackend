/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.entities;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mathews Jovel Castro
 */
@XmlRootElement
public class Instituciones {
//Definicion de variables privadas de la clase Instituciones---------------------
    private Integer id;
    private String nombre;
    private String comuna;
    private String codigoDane;
    private String especialidad;
    private String nivelEducativo;
    private String barrio;
    private String direccion;
    private String telefono;
//--------------------------------------------------------------------------
    
    public Instituciones(){
    }

    public Instituciones(Integer id, String nombre, String comuna, String codigoDane, String especialidad, String nivelEducativo, String barrio, String direccion, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.comuna = comuna;
        this.codigoDane = codigoDane;
        this.especialidad = especialidad;
        this.nivelEducativo = nivelEducativo;
        this.barrio = barrio;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getCodigoDane() {
        return codigoDane;
    }

    public void setCodigoDane(String codigoDane) {
        this.codigoDane = codigoDane;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getNivelEducativo() {
        return nivelEducativo;
    }

    public void setNivelEducativo(String nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}


