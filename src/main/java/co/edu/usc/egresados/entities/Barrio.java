/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.entities;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
public class Barrio {
    private String barId;
    private String barNombre;
    
    public Barrio() {  
}
    public Barrio(String nBarId, String nBarNombre ){
        this.barId = nBarId;
        this.barNombre = nBarNombre;
    }

    public String getBarId() {
        return barId;
    }

    public void setBarId(String barId) {
        this.barId = barId;
    }

    public String getBarNombre() {
        return barNombre;
    }

    public void setBarNombre(String barNombre) {
        this.barNombre = barNombre;
    }
}
