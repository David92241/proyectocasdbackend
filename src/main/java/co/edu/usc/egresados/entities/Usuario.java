/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usc.egresados.entities;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mathews Jovel Castro
 */
@XmlRootElement
public class Usuario {
    //Definicion de variables privadas de la clase Usuario---------------------
    private Integer id;
    private String nombre;
    private String contrasena;
    private String tipo_usuario;
    private Integer id_sede_casd;
    private String token;
    //------------------------------------------------------------------------------
    
    public Usuario(){
    }
    
    //Metodo constructor de la clase Usuario--------------------------------------------------------------------
    public Usuario(Integer id, String nombre, String contrasena, String tipo_usuario, Integer id_sede_casd){
        this.id=id;
        this.nombre=nombre;
        this.contrasena=contrasena;
        this.tipo_usuario=tipo_usuario;
        this.id_sede_casd=id_sede_casd;
    }
    //---------------------------------------------------------------------------------------------------------------
    
    // Metodos get y set de la varible id-------------------------------------
    /***
     * metodo que retorna la id del usuario
     * @return la id con la que esta identificado el usuario 
     */
    public Integer getId(){
        return id;
    }
    /***
     * metodo que le asigna una id al usuario por la id pasada por parametro
     * @param id asignada 
     */
    public void setId(Integer id){
        this.id=id;
    }
    //-----------------------------------------------------------------------------
    
     // Metodos get y set de la varible nombre-------------------------------------
    /***
     * metodo que retorna el nombre del usuario
     * @return el nombre del usuario 
     */
    public String getNombre(){
        return nombre;
    }
    /***
     * metodo que asigna un nombre al usuario con el nombre pasado por parametro
     * @param nombre asignado al usuario 
     */
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    //-----------------------------------------------------------------------------
    
    // Metodos get y set de la variable contraseña-----------------------------------
    /***
     * Metodo que retorna la contraseña del usuario
     * @return la contraseña del usuario 
     */
    public String getContrasena(){
        return contrasena;
    }
    /***
     * metodo que asigna una contraseña al usuario con la contraseña pasada por parametro
     * @param contrasena asignada al usuario 
     */
    public void setContrasena(String contrasena){
        this.contrasena=contrasena;
    }
    //--------------------------------------------------------------------------------------
    
    // metodos get y set de la variable tipo_usuario----------------------------------------------
    /***
     * metodo que retorna el tipo de privilegio de usuario asignado a un usuario
     * los privilegios de usuario son: administrador y operador
     * @return el tipo de privilegio de usuario 
     */
    public String getTipo_usuario(){
        return tipo_usuario;
    }
    /***
     * metodo que asigna un tipo de usuario con la variable de usuario pasada por parametro
     * @param tipo_usuario asignado a un usuario
     */
    public void setTipo_usuario(String tipo_usuario){
        this.tipo_usuario=tipo_usuario;
    }
   //---------------------------------------------------------------------------------------
    
    // metodos get y set de la id_sede_casd-----------------------------------------------------
    /***
     * metodo que retorna la id de la sede en la que estan asociados los usuarios
     * @return la id de la sede 
     */
    public Integer getId_sede_casd(){
        return id_sede_casd;
    }
    /***
     * metodo que le asgina la id a la sede CASD con la id pasada por parametro
     * @param id_sede_casd asignada a la sede CASD correspondiente
     */
    public void setId_sede_casd(Integer id_sede_casd){
        this.id_sede_casd=id_sede_casd;
    }
    //----------------------------------------------------------------------------------

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
}

